package coinpurse;
 
import java.util.ArrayList;
import java.util.Collections;
/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Natthapol Kluaythes 5710546569
 */
public class Purse {
    /** Collection of coins in the purse. */
	private ArrayList<Coin> coins;
    
    /** Capacity is maximum NUMBER of coins the purse can hold.
     *  Capacity is set when the purse is created.
     */
    private int capacity;
    
    /** 
     *  Create a purse with a specified capacity.
     *  @param capacity is maximum number of coins you can put in purse.
     */
    public Purse( int capacity ) {
    	this.capacity = capacity;
    	coins = new ArrayList<Coin>();
    }

    /**
     * Count and return the number of coins in the purse.
     * This is the number of coins, not their value.
     * @return the number of coins in the purse
     */
    public int count() { 
    	return coins.size();
    }
    
    /** 
     *  Get the total value of all items in the purse.
     *  @return the total value of items in the purse.
     */
    public double getBalance() {
    	double total = 0;
    	for(int i =0;i<coins.size();i++){
    		total += coins.get(i).getValue();
    	}
    	return total;
    }

    
    /**
     * Return the capacity of the coin purse.
     * @return the capacity
     */
    public int getCapacity() { return this.capacity; }
    
    /** 
     *  Test whether the purse is full.
     *  The purse is full if number of items in purse equals
     *  or greater than the purse capacity.
     *  @return true if purse is full.
     */
    public boolean isFull() {
    	if (capacity == coins.size())
    		return true;	
        return false;
    }

    /** 
     * Insert a coin into the purse.
     * The coin is only inserted if the purse has space for it
     * and the coin has positive value.  No worthless coins!
     * @param coin is a Coin object to insert into purse
     * @return true if coin inserted, false if can't insert
     */
    public boolean insert( Coin coin ) {
    	if(isFull())
    		return false;
    	else
    		coins.add(coin);
    		return true;
    }
    
    /**  
     *  Withdraw the requested amount of money.
     *  Return an array of Coins withdrawn from purse,
     *  or return null if cannot withdraw the amount requested.
     *  @param amount is the amount to withdraw
     *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
     */
    public Coin[] withdraw( double amount ) {
    	if(amount <= 0){
    		return null;
    	}
    	
    	Collections.sort(coins);
    	ArrayList<Coin> Keep = new ArrayList<Coin>();
    	
    	for(int i = coins.size()-1;i>=0;i--){
    		if(amount>=coins.get(i).getValue()){
    			Keep.add(coins.get(i));
    			amount-=coins.get(i).getValue();
    		}
    	}
    	if ( amount > 0 )
		{
			return null;
		}
		Coin[] re = new Coin[Keep.size()];
		Keep.toArray(re);
		
		for(int i = 0 ; i < re.length ; i++){
			this.coins.remove(re[i]);
		}

        return re;
	}
        
	   /*
		* One solution is to start from the most valuable coin
		* in the purse and take any coin that maybe used for
		* withdraw.
		* Since you don't know if withdraw is going to succeed, 
		* don't actually withdraw the coins from the purse yet.
		* Instead, create a temporary list.
		* Each time you see a coin that you want to withdraw,
		* add it to the temporary list and deduct the value
		* from amount. (This is called a "Greedy Algorithm".)
		* Or, if you don't like changing the amount parameter,
		* use a local total to keep track of amount withdrawn so far.
		* 
		* If amount is reduced to zero (or tempTotal == amount), 
		* then you are done.
		* Now you can withdraw the coins from the purse.
		* NOTE: Don't use list.removeAll(templist) for this
		* becuase removeAll removes *all* coins from list that
		* are equal (using Coin.equals) to something in templist.
		* Instead, use a loop over templist
		* and remove coins one-by-one.		
		*/

    /** 
     * toString returns a string description of the purse contents.
     * It can return whatever is a useful description.
     */
    public String toString() {
    	return "This purse can refill coin.";
    }
}
