package coinpurse;

/**
 * a coin with a monetary value
 * @author Natthapol Kluaythes 5710546569
 */
public class Coin implements Comparable<Coin>{

    /** Value of the coin. */
    private double value;
    
    public Coin( double value ) {
        this.value = value;
    }
    
//TODO Write a getValue() method and javadoc.
    /**
     * Get the Coin 's value.
     * @return the value of this coin
     */
    public double getValue( ) { return this.value; }
    /**
	 * Compare Coin's value.
	 * They are equal if the value matches.
	 * @param other is another coin to compare to this one.
	 * @return true if the value is same, false otherwise.
	 */
    public boolean equals(Object obj){
    	if(obj == null) return false;
    	if(obj.getClass() != this.getClass())
    		return false;
    	Coin other = (Coin) obj;
    	if(value == other.getValue())
    		return true;
    	return false;
    }
    /**
     * Compare Coin's value.
     * @return if this value less than Object in parameter it will return -1.
     * @return if value is equals it will return-1, 1 otherwise
     */
    public int compareTo(Coin obj){
    	
    	if (this.value<obj.getValue())
    		return -1;
    	else if(this.value==obj.getValue())
    		return 0;
    	else 
    		return 1;
    	
    }
    /**
	 * Get a string representation of this Purse.
	 * @return the coin's value of this Purse.
	 */
    public String toString(){
    	return this.getValue()+" baht coin";
    }
}

